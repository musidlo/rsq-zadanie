package com.musidlowski.mikolaj.rsqFirstTry.service

import com.musidlowski.mikolaj.rsqFirstTry.entity.Visit
import java.time.LocalDateTime
import java.util.*

interface VisitService {

    fun findAllVisits(): List<Visit>
    fun findVisitById(id: Long): Visit?
    fun findVisitsByPatientId(patientId: Long): List<Visit>
    fun saveVisit(visit: Visit): Visit
    fun updateVisitTimeById(id: Long, newDate: LocalDateTime): Boolean
    fun deleteVisitById(id: Long): Boolean
}