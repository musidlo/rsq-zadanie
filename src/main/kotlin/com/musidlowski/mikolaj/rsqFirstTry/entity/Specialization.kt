package com.musidlowski.mikolaj.rsqFirstTry.entity

enum class Specialization(val id: Int) {
    PHYSIOTHERAPIST(0),
    DENTIST(1)
}