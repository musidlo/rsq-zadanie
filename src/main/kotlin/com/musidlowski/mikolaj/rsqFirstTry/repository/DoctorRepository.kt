package com.musidlowski.mikolaj.rsqFirstTry.repository

import com.musidlowski.mikolaj.rsqFirstTry.entity.Doctor
import org.springframework.data.jpa.repository.JpaRepository

interface DoctorRepository : JpaRepository<Doctor, Long>