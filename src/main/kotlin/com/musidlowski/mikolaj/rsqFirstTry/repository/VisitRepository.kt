package com.musidlowski.mikolaj.rsqFirstTry.repository

import com.musidlowski.mikolaj.rsqFirstTry.entity.Visit
import org.springframework.data.jpa.repository.JpaRepository

interface VisitRepository : JpaRepository<Visit, Long> {
    fun findByPatient_Id(patientId: Long): List<Visit>
}