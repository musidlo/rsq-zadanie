package com.musidlowski.mikolaj.rsqFirstTry.repository

import com.musidlowski.mikolaj.rsqFirstTry.entity.Patient
import org.springframework.data.jpa.repository.JpaRepository

interface PatientRepository : JpaRepository<Patient, Long>