package com.musidlowski.mikolaj.rsqFirstTry.serviceImpl

import com.musidlowski.mikolaj.rsqFirstTry.entity.Doctor
import com.musidlowski.mikolaj.rsqFirstTry.repository.DoctorRepository
import com.musidlowski.mikolaj.rsqFirstTry.service.DoctorService
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.util.*

@Service
class DoctorServiceImpl (private val repository: DoctorRepository) : DoctorService  {

    override fun findDoctorById(id: Long): Doctor? = repository.findByIdOrNull(id)

    override fun saveDoctor(doctor: Doctor): Doctor = repository.save(doctor)

    override fun updateDoctor(id: Long, doctor: Doctor): Boolean {
        findDoctorById(id) ?: return false
        doctor.id = id
        saveDoctor(doctor)
        return true
    }

    override fun deleteDoctorById(id: Long): Boolean {
        try {
            repository.deleteById(id)
        } catch (exception: EmptyResultDataAccessException) {
            return false
        }
        return true
    }
}