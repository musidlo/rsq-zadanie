package com.musidlowski.mikolaj.rsqFirstTry.serviceImpl

import com.musidlowski.mikolaj.rsqFirstTry.entity.Visit
import com.musidlowski.mikolaj.rsqFirstTry.repository.VisitRepository
import com.musidlowski.mikolaj.rsqFirstTry.service.VisitService
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.util.*

@Service
class VisitServiceImpl(private val repository: VisitRepository) : VisitService {

    override fun findAllVisits(): List<Visit> = repository.findAll()

    override fun findVisitById(id: Long): Visit? = repository.findByIdOrNull(id)

    override fun findVisitsByPatientId(patientId: Long): List<Visit> = repository.findByPatient_Id(patientId)

    override fun saveVisit(visit: Visit): Visit = repository.save(visit)

    override fun updateVisitTimeById(id: Long, newDate: LocalDateTime): Boolean {
        val foundVisit = findVisitById(id) ?: return false
        foundVisit.date = newDate
        saveVisit(foundVisit)
        return true
    }

    override fun deleteVisitById(id: Long): Boolean {
        try {
            repository.deleteById(id)
        } catch (exception: EmptyResultDataAccessException) {
            return false
        }
        return true
    }
}
